La verdad es que son muy similares, el IDE VisualStudio Code, al hacer una clase con el metodo Prototype me da la opcion de convertirla a la nueva version de tipo Clase.  
Las clases fueron implementados no hace mucho al lenguaje de JS, te permiten heredar otras clases con extends. Anterior a eso, para simular la herencia utilizaban prototype.  
Las clases son algo mas lentas respecto a prototype. Para proyectos muy largos es mas convenientes usar prototype.



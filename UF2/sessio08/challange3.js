console.log("\n------ Clases / Herencia ------")
// class EV extends Automovil {

//     constructor(marca, velocidad, bateria) {
//         super(marca, velocidad);
//         this.bateria = bateria;
//     }

//     /**
//      * Carga la bateria hasta el parametro indicado
//      * @param chargeTo 
//      */
//     chargeBattery(chargeTo) {
//         this.bateria = chargeTo;
//     }

//     /**
//      * Acelera en 20 k/h y consume 1% de bateria
//      */
//     accelerate() {
//         this.velocidad += 20;
//         this.bateria -= 1;
//         console.log(`${this.marca} going at ${this.velocidad}, with a charge of ${this.bateria}`);
//     }

//     /**
//      * Sobreescribir(?) la funcion mostrarCoche() de Automovil
//      */
//     mostrarCoche() {
//         console.log("Marca: " + this.marca + " Velocidad: " + this.velocidad + " bateria:" + this.bateria)
//     }
// }

//const car3 = new EV("Testal", 100, 40);
// car3.mostrarCoche();
// car3.chargeBattery(60);
// car3.mostrarCoche();
// console.log("Aceleramos accelerate()")
// car3.accelerate();

const car = function (marca, velocidad) {
    this.marca = marca;
    this.velocidad = velocidad;
}

car.prototype.mostrar = function () {
    console.log("Marca: " + this.marca + " velocidad:" + this.velocidad);
}

const EV = function (marca, velocidad, bateria) {
    car.call(this, marca, velocidad);
    this.bateria = bateria;
}

EV.prototype = Object.create(car.prototype);

EV.prototype.chargeBattery = function (chargeTo) {
    this.bateria = chargeTo;
}

EV.prototype.accelerate = function () {
    this.velocidad += 20;
    this.bateria -= 1;
    console.log(`${this.marca} going at ${this.velocidad}, with a charge of ${this.bateria}`);
}

const car4 = new EV("Tesla", 100, 40);
car4.mostrar();
console.log("Bateria: " + car4.bateria);
car4.accelerate();


## Clases

Se podría decir que son los moldes que definen la forma y acciones que tendrá un objeto que se cree a partir de este. El numero y tipo de atributos que tendrá. De igual forma en los métodos(funciones).
Las clases también se pueden definir a con la visibilidad que tendrá(public, private)

  

## Objetos

Son una colección de propiedades (atributos y métodos). Este puede ser creado a partir de una clase o no. Si son creados a partir de una clase, en el momento de instanciarlos, tiene que ser colección los atributos definidos por la clase.

  

## Eventos y propiedades

  

### Propiedades / Atributos

Las propiedades son variables contenidas en la clase, cada instancia del objeto tiene dichas propiedades. Las propiedades deben establecerse a la propiedad prototipo de la clase (función), para que la herencia funcione correctamente.  Para trabajar con propiedades dentro de la clase se utiliza la palabra reservada "this" , que se refiere al objeto actual.

El  acceso  (lectura  o  escritura)  a  una  propiedad  desde  fuera  de  la  clase  se  hace  con  la  sintaxis: NombreDeLaInstancia.Propiedad.
Es la misma sintaxis utilizada por Java y algunos lenguajes más. (Desde dentro de la clase la sintaxis es this.Propiedad que se utiliza para obtener o establecer el valor de la propiedad).

  

### Eventos / Métodos

Los métodos siguen la misma lógica que las propiedades, la diferencia es que son funciones y se definen como funciones. Llamar a un método es similar a acceder a una propiedad, pero se agrega () al final del nombre del método, posiblemente con argumentos.  

En JavaScript los métodos son objetos como lo es una función normal y se vinculan a un objeto como lo hace una propiedad, lo que significa que se pueden invocar desde "fuera de su contexto"

## Encapsulamiento
Es limitar la visibilidad de los atributos/métodos de una clase. Para que estas no puedan ser vistas y/o modificadas desde fuera.
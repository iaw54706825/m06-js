# 1. Documentar Javascript versión

- “var” - “let”

ES5
La palabra reservada “var” permite a la variable ser mucho mas visible en bloque de código. Este tipo de variable permitía la redeclaración. No genera ningún tipo de error por volver a declarar dicha variable

ES6
La palabra reservada “let” limita la visibilidad de la variable en el bloque de código. Por ejemplo, si una variable se declara dentro de una función, solo sera visible dentro de esta.
Esta varbiale no se puede redeclarar ya que genera un error al redeclarar

- const (constantes)

ES5

En esta versión no existía la manera oficial de declarar una constante para que sea utilizada en modo luctura. Para poder hacer esto, a una variable había que declararle un objetos, y las propiedades no se podían modificar.

ES6

En esta versión, la notación “const” es suficiente para declarar la variable en modo solo lectura.

- Funciones

ES5 y anteriores

Es obligatorio escribir la palabra reservada “function”.

ES6

Se utiliza la notación de funciones flecha (arrow functions).
Ejemplos: var nombreFUncion = () =&gt;x+y;

- Getters y Setters

A partir de la versión ES6, no era necesario utilizar la palabra reservada function() para crear los getters y setters. Ejemplo: set SetX() {this.x=x};

- Clases
A partir de ES5 JavaScript incorpora clases con todo lo que conlleva, como por ejemplo, la herencia. Son muy parecidas a las funciones constructoras de objetos que realizábamos en el estándar anterior.


Con ES6 podemos interpolar Strings de una forma más sencilla que como estábamos haciendo hasta ahora. Por ejemplo:let nombre1 = "JavaScript";
let nombre2 = "awesome";
console.log(‘Sólo quiero decir que ${nombre1} is ${nombre2’);

- Valores por defecto
Otra de sus novedades es asignar valores por defecto a sus variables, que se pasan por parámetros en las funciones. Antes teníamos que comprobar si la variable ya tenía un valor. Ahora con ES6 se la podremos asignar según creemos la función:


# 2. Comparar Cadenas

- 1r “==”
Comparador de igualdad abstracta. No es estricta, por ejemplo si hiciéramos 1==”1” nos devolvería “true”.

- 2r “===”
Comparador de igualdad estricta, En este caso se compara el valor y tipo de datos, por ejemplo si hiciéramos 1 === “1” nos devolvería “false”

- 3r “localCompare”
El método localeCompare() devuelve un número que indica si la cadena de caracteres actual es anterior, posterior o igual a la cadena pasada como parámetro, en orden lexicográfico.
Ejemplos: 'a'.localeCompare('a'); // 0

- 4r Crear una función con charAt()
Una opción seria crear un bucle que recorra las dos cadenas, y comparar letra por letra con un char.At() que sea iguales.

- 5t Crear una función con index.Of() y length


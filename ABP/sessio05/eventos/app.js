const app = Vue.createApp({
    data() {
      return {
        interesClient: 'Intereses del cliente',
        interesVendedor: 'Interes vendedor',
        checked: '',
        nombre: '',
        cognom: '',
        email: '',
        info: ''
      };
    },
    methods: { 
     interes(param) {
       if (param == 'c') {
         this.checked = this.interesClient;
       } else if (param == 'v') {
         this.checked = this.interesVendedor;
       }
     },
     mensaje(){
       if (this.nombre == '' || this.cognom == '' || this.email == '') {
        this.info = "Faltan campos por rellenar";
       }     
     } 
    }
  });
  
  app.mount('#form');

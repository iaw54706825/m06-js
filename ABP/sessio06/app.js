const app = new Vue({
    el: '#app',
    data: {
        titulo: "CRUD Vue",
        tarea: "",
        listaTareas: []
    },
    methods: {
        agregarTarea: function() {
            this.listaTareas.push({
                name: this.tarea,
                estado: false
            });
            this.tarea = "";
        },
        tareaHecha: function(index) {
            this.listaTareas[index].estado = true;
        },
        eliminarTarea: function(index) {
            this.listaTareas.splice(index,1);
        }
    }
})
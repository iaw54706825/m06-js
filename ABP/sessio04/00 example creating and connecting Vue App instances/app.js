const app = Vue.createApp({
  data() {  // sempre hem de fer servir la funció de Vue que es diu data() .. que busca aquesta propietat
    return {
      courseGoalA: 'Finish the course and learn Vue!',
      courseGoalB: 'Master Vue and build amazing apps!',
      vueLink: 'https://vuejs.org/',
      proves: "Això es una prova donem valor sense fer servir funcions..."
    };
  },
  methods: {
    outputGoal() {
      const randomNumber = Math.random();
      if (randomNumber < 0.5) {
        return this.courseGoalA;
      } else {
        return this.courseGoalB;
      }
    }
  }
})

app.mount('#user-goal'); //amb això connectem la nosrtra aplicacio de JS amb la seccio del html.

/*
1. Afegirem al HTML un formulari amb 3 input text amb les seguents dades: nom, cognoms, mail. 

2. Afegirem al formulari un radiobutton pel tipus d'usuari: amb dos valors: client, venedor.

3. Afegirem al formulari dos checkboxs. Un amb interessos del client, i un altre amb 
interessos del venedor. 

4. Afegirem un event amb vue, al clickar al radio button, si està seleccionat client 
escriurem els interessos client, si està seleccionat venedor, mostrem interessos venedor. 
( aquesta informació sortirà a la part inferior del HTML, podeu fer servir una capa ). 

5. Crearem un botó enviar: Aquest botó cridarà una funció que enviarà el formulari 
( submit). Però de moment evitem enviar el formulari i en el seu defecte mostrem un missatge. 
*/
const app = Vue.createApp({
    data() {  // sempre hem de fer servir la funció de Vue que es diu data() .. que busca aquesta propietat
      return {
        // declare value linked to input text content, make input text empty
        // it will also send and recieve text from the item
        // right now it is sending '' to the input element
        // it is also being declared
        // array to store the list item's text content
        client : false,
        vendor : false,
      }
    },
    methods: {
      // returns if client is already displayed
      // shows client if both are hidden
      // hides vendor and shows client otherwise
      cli() {
        if (this.client) {return;}
        if (!this.client && !this.vendor) {
          this.client = true;
        } else {
          this.vendor = false;
          this.client = true;
        }
      },
      // returns if vendor is displayed
      // shows vendor if both are hidden
      // hides client and shows vendor otherwise
      ven() {
        if (this.vendor) {return;}
        if (!this.vendor && !this.client) {
          this.vendor = true;
        } else {
          this.client = false;
          this.vendor = true;
        }
      },
      // method called on submit
      mostraAlert() {
          alert('Formulari enviat!');
      }
    }
  })
  app.mount('#myApp') //amb això connectem la nosrtra aplicacio de JS amb la seccio del html.
  
  
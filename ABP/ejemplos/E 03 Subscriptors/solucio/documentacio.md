# Documentació  
  
## Diferencia entre  v-bind i  v-on
### v-bind
Pot ser ressumit a `:style` o `:class`, `:src`, etc.
Affecta a les propietats, com ara href, class, style, dels elements. Pot reaccionar a un attribut o a un computed element/method.  
Exemple amb atribut:  
```
div v-bind:class="[isActive ? activeClass : '', errorClass]"></div>
// tindra la class activeClass cuan el atribut isActive sigui true, errorClass de lo contrari
```
Exemple amb computed:  
```
<div v-bind:class="classObject"></div>

data: {
  isActive: true,
  error: null
},
computed: {
  classObject: function () {
    return {
      active: this.isActive && !this.error,
      'text-danger': this.error && this.error.type === 'fatal'
    }
  }
}
```
----
### v-on  
Se ocupa de eventos, como ahora keydown, click, submit, etc. En el caso de submit se puede nelazar con `.prevent` para evitar enviar el fomulario.  
  
----
## Events modifiers. Quins tipus de modificadors d'events tenim?. 
* `.prevent`
* `.exact`
* `.passive`
* `.capture`
* `.self`
* `.once`
* `.stop`
  

`.prevent` : evita comportamientos por defecto como un refresco de la pagina en un submit.  
`.exact` : hace que el conjunto de eventos sea exactamente el descrito, por ejemplo ` @click.ctrl.exact="onCtrlClick"` llamara a onCtrlClick (metodo o computed) exclusivemente cuando solo se combinen click con la tecla ctrl, si picamos otra tecal mas por ejemplo el evento no se llamara.  
`. passive` : `El modificador .passive es especialmente útil para mejorar el rendimiento en dispositivos móviles.` Espera a que se complete un evento.  
`.capture` : `un evento dirigido a un elemento interno se maneja aquí antes de ser manejado por ese elemento`  
`.self` : `El orden es importante cuando se usan modificadores porque el código relevante se genera en el mismo orden. Por lo tanto, el uso de v-on: click.prevent.self evitará todos los clics mientras que v-on: click.self.prevent solo evitará clics en el elemento en sí.`  
`.once` : el evento se activara maximo una vez, no importa cuantas veces cumplamos la condiciónd e activar el evento.  
`.stop` : parara la propagación del elemento.  
----  
  
## Perque serveix el prevent en el botó del submit? `<form v-on:submit.prevent="submitForm">`  
Evita que la pagina se refresque como es su comportamiento por defecto en un boton tipo submit.   
----
## v-on:click.left quines altres modificador d'events, tenim en el click .. i que fan?  
listeners only when the corresponding modifier key is pressed:  

    .ctrl
    .alt
    .shift
    .meta  

Estos detectan teclas pulsadas junto a @click  
    .exact  

Controlara que la combinación de eventos (por ejemplo ctrl + click) sea exacta y no se sumen otros modificadores (por ejemplo shift)  
  
    .left  
    .right  
    .middle  
  
Corresponden a los botones del mouse.  

    .enter  
    .tab  
    .delete (captures both “Delete” and “Backspace” keys)  
    .esc  
    .space  
    .up  
    .down  
    .left  
    .right    

Mas teclas  
    .prevent  

Detiene un comportamiento predefinido de un elemento.  
----
## Que fa el modifficador v-on:Keyup ?
Detecta cuando una tecla ha sido pulsada y se deja de pulsar, es cecir, cuando levantamos el dedo de una tecla pulsada.  


import Vue from "vue";
import { createRouter, createWebHistory } from 'vue-router'

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "inicio",
      component: () => import("./components/Inicio")
    },
    {
      path: "/listar",
      name: "listar",
      component: () => import("./components/listar")
    },
    {
      path: "/add",
      name: "add",
      component: () => import("./components/CrearTutorial")
    }
  ]
});

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})
# Definicion y analisis

## Forma de trabajo
Siguiendo las recomendaciones de la profesora, el proyecto se hara de forma individual.

## Descripcion Backend
El proyecto se centrara en poder administrar la lista de usuarios registrados en la pagina web 'highcode'.
Los usuarios se dividiran en dos roles de tipo "alumno" y "profesor". Por cada usuario tendremos 3 opciones:
- Cambiar el rol del usuario.
- Eliminar usuario.
- Visualizar informacion.

## Recursos
El backoffice(?) se desarrollara con las siguientes herramientas:
- VisualStudio y sus extensiones.
- Vue-Cli.
- Git(Ramas y versiones).
Enlace al git:[Enlace git](https://gitlab.com/iaw54706825/m06-js/-/tree/vue-project/ABP/project)


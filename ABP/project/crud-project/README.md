# Instalar el proyecto

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Posibles erroes
```
- Reinstalar la carpeta `node_modules`

# Instalar el servidor Json

- Dentro de la carpeta fakeServe seguir los pasos del moodle
1 Install JSON Server de manera global al nostre PC, `npm install -g json-server`
2 Installem el JSON server a un projecte local , i Per evitar que ens faci totes les preguntes fem directament:  `npm install -y` ->> Creo que aqui es `npm init -i`
3 Instalem les dependecies al nostre fake server, `npm i json-server`
4 Arrancar el servidor JSON  `json-server --watch ./db/db.json`
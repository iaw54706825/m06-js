## Carpeta public
Esta carpeta se utiliza para almacenar ficheros estáticos que no serán procesados por el framework. Es un lugar ideal para almacenar ficheros estáticos como robots.txt, favicon.ico, archivos HTML finales como el index.html, o imágenes o recursos estáticos. Estos archivos siempre serán públicos y cualquier usuario podrá acceder a ellos, ya que no pasa antes por el framework.

## Carpeta componentes
En el interior de la carpeta **src** se encuentra la carpeta de componentes.
Una de las carpetas más importantes de nuestro proyecto Vue. En ella colocaremos los componentes .vue que iremos creando durante nuestro proyecto. Los componentes .vue son archivos que contienen el HTML, CSS y Javascript que está relacionado con una determinada parte de la página, como podría ser un botón, un panel desplegable o un comparador de imágenes.

## Carpeta src 
Es la carpeta mas importante. En ella se almacena el codigo fuente de nuestro proyecto, el que estaremos modificando desde nuestro editor de texto o IDE. Es muy importante tener presente que dentro de src siempre vamos a encontrar los archivos originales sin procesar.Mientras, en otra carpeta fuera de src/, denominada comunmente dist/ o build/ se suelen almacenar los archivos finales procesados, como .js y .css, por ejemplo, que son los archivos procesados finales que irán desplegados en el servidor o web en producción definitiva.

## Fichero main.js
En Vue, encontraremos siempre un fichero main.ts (TypeScript) o main.js (Javascript). Se trata del fichero principal que arranca el proyecto Vue y que se insertará en la plantilla index.html que se incluye en la carpeta public/.
Este archivo se encargará de cargar Vue y todos sus plugins asociados. 

## Fichero App.vue
Los ficheros .vue son los denominados SFC de Vue (Single File Components), se trata de un archivo especial de Vue, muy similar a un .html que incluye 3 etiquetas HTML especiales: <template>, <script> y <style>. A primera vista, parecen ficheros html.


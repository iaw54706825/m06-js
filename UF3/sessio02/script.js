//Ejercicio 4, validar tipo de documento
const validarDocumento = () => {
    let tipoDocumento = document.getElementById("identificacio").value;
    let numDocumento = document.getElementById("numDocumento");
    if (tipoDocumento == 'dni' && numDocumento.value.length == 9) {
        if (isNaN(numDocumento.value.charAt(numDocumento.value.length - 1))) {
            alert("DNI correcto");
        } else {
            alert("Error dni");
        }
    } else if (tipoDocumento == 'nie' && numDocumento.value.length == 8) {
        if (isNaN(numDocumento.value.charAt(0))) {
            alert("NIE correcto");
        }
        else {
            alert("Error nie");
        }
    } else {
        alert("Error");
    }
}
document.getElementById("numDocumento").addEventListener('focusout', validarDocumento);


//Ejercicio 6, validar edad
const validarEdad = () => {
    let nacimiento = document.getElementById("nacimiento").value;
    let year = nacimiento.split("-")[0];
    let yearActual = new Date();
    if (yearActual.getFullYear() - year < 18) {
        alert("Menor de edad")
    }
}
document.getElementById("nacimiento").addEventListener('mouseout', validarEdad);


//Datos de contacto
//Ejercicio 3, vive en BCN
const viuBcn = () => {
    document.querySelectorAll('input[name="viuBarceloa"]').forEach((el) => {
        if (el.checked) {
            if (el.value == "si") {
                let a = document.getElementById("opcional").style.display = 'block';
            } else {
                let a = document.getElementById("opcional").style.display = 'none';

            }
        }
    })
}
a = document.querySelectorAll('input[name="viuBarceloa"]').forEach((el) => {
    el.addEventListener('click', viuBcn)
})


//Ejercicio DOM Sessio 04/05
const ejercicios = () => {
    addDocument();
    listaEstudios();
    listaLaboral();
    listaNacionalidad();
    listaPaisesAxios(); // <----------AXIOS!!
    direcciones();
}

//Añadir pasaporte
const addDocument = () => {
    let documento = document.getElementById("identificacio");
    let pasaporte = document.createElement("option");
    pasaporte.text = "Pasaporte";
    // documento.add(pasaporte);
    documento.appendChild(pasaporte);
}

//Añadir lista de estudios
const listaEstudios = () => {
    let listaEstudios = `Educació secundària obligatòria, Programes de formació i inserció, 
    Batxillerat, Formació professional, Ensenyaments artístics superiors, Ensenyaments esportius,
    Idiomes a les EOI, Educació d'adults, Cursos per accedir a cicles, Itineraris formatius específics, Estudis estrangers`;
    let arrEstudios = listaEstudios.split(",");
    let estudios = document.getElementById('estudis');
    arrEstudios.forEach(element => {
        let opcion = document.createElement('option');
        opcion.text = element.split();
        estudios.appendChild(opcion);
    });
}

//Lista situacion laboral
const listaLaboral = () => {
    let listaLaboral = `estudiant, aturat, treball compte propi, treball compte aliè, pensionista.`
    let arrLista = listaLaboral.split(",");
    let laboral = document.getElementById('laboral');
    arrLista.forEach(element => {
        let opcion = document.createElement('option');
        opcion.text = element.split();
        laboral.appendChild(opcion);
    });
}

//Nacionalidad
const listaNacionalidad = () => {
    let listaNacionalidad = `espanyola, UE, FUE`
    let arrLista = listaNacionalidad.split(",");
    let nacionalidad = document.getElementById('nacionalitat');
    arrLista.forEach(element => {
        let opcion = document.createElement('option');
        opcion.text = element.split();
        nacionalidad.appendChild(opcion);
    });
}

//Lista pais con Axios
const listaPaisesAxios = () => {
    axios.get('https://restcountries.eu/rest/v2/all')
        .then(res => {
            listaPaises = res.data;
            let paises = document.getElementById('pais')
            console.log(paises);
            listaPaises.forEach(element => {
                let opcion = document.createElement('option');
                opcion.text = element.name;
                paises.appendChild(opcion);
            });
        })
        .catch(err => console.log(err));
}

//Direccion
const direcciones = () => {
    /** Definim la variable de mapa */
    var mapa1, cercadorTipusVia, cercadorCarrer, cercadorPortal;

    /** Establim l'entorn en mode mòbil */
    geoBCN.esMobil = true;

    /** Programem la inicialització de l'entorn */
    document.addEventListener('geobcn-ready', function () {

        cercadorTipusVia = new geoBCN.Html.InputAutocomplete({
            inputId: 'tipusVia',
            label: 'Tipus Via',
            combobox: true,
            origen: function (request, response) {
                geoBCN.Cercadors.cercaTipusVia({
                    nom: cercadorTipusVia.getText(),
                    nomesCamps: ['nom', 'codi'], //Solament necessitem aquests camps de retorn
                    resultat: function (data) {
                        response(geoBCN.Utils.mapResults(data.resultats, function (item) {
                            return {
                                label: item.nom,
                                value: item.codi
                            };
                        }));
                    }
                });
            },
            esborrat: function () {
                $('#out').html('');
            },
            seleccio: function () {
                $('#out').html('');
            }
        });

        /*Autocomplete de carrers*/
        cercadorCarrer = new geoBCN.Html.InputAutocomplete({
            inputId: 'carrer',
            label: 'Carrer',
            master: cercadorTipusVia,   //El carrer depén del tipus via
            masterRequired: false, 	  //El tipus NO es obligatori
            origen: function (request, response) {
                geoBCN.Cercadors.cercaCarrers({
                    id_tipus_via: cercadorTipusVia.getCodi(),
                    nom: cercadorCarrer.getText(),
                    nomesCamps: ['Nom18', 'Codi'], //Solament necessitem aquests camps de retorn
                    resultat: function (data) {
                        response(geoBCN.Utils.mapResults(data.resultats, function (item) {
                            return {
                                label: item.nom18,
                                value: item.codi
                            };
                        }));
                    }
                });
            },
            esborrat: function () {
                $('#out').html('');
            },
            seleccio: function () {
                $('#out').html('');
            }
        });

        cercadorPortal = new geoBCN.Html.InputAutocomplete({
            inputId: 'portal',
            label: 'Portal',
            master: cercadorCarrer,   //El portal depèn del carrer
            masterRequired: true, 	//El carrer es obligatori
            origen: function (request, response) {
                geoBCN.Cercadors.cercaPortals({
                    id_via: cercadorCarrer.getCodi(),
                    numero: cercadorPortal.getText(),
                    resultat: function (data) {
                        response(geoBCN.Utils.mapResults(data.resultats, function (item) {
                            return {
                                label: item.numeracioPostal,
                                value: item
                            };
                        }));
                    }
                });
            },
            esborrat: function () {
                $('#out').html('');
            },
            seleccio: function (ev, adr) {
                var coordTressor = geoBCN.Utils.projectaCoordenades('EPSG:25831', 'TRESOR', adr.localitzacio.x, adr.localitzacio.y);
                var text = '';
                var tab = "    ";
                text += 'Adreça seleccionada:<br/><br/>' +
                    tab + '- Coordenades (' + adr.localitzacio.proj + '): ' + adr.localitzacio.x + ', ' + adr.localitzacio.y + '\n' +
                    tab + '- CoordenadesTressor (TRESOR): ' + coordTressor[0] + ', ' + coordTressor[1] + '\n' +
                    tab + '- Tipus Via: ' + adr.carrer.tipusVia.nom + ' (' + adr.carrer.tipusVia.abreviatura + ')\n' +
                    tab + '- Tipus Via Id: ' + adr.carrer.tipusVia.codi + '\n' +
                    tab + ' - Carrer - nom: ' + adr.carrer.nom + '\n' +
                    tab + ' - Carrer - nom18: ' + adr.carrer.nom18 + '\n' +
                    tab + ' - Carrer - nom27: ' + adr.carrer.nom27 + '\n' +
                    tab + ' - Carrer - nomComplet: ' + adr.nomComplet + '\n' +
                    tab + ' - numeroInicial: ' + adr.numeroPostalInicial + adr.lletraPostalInicial + '\n' +
                    tab + ' - numeroFinal: ' + adr.numeroPostalFinal + adr.lletraPostalFinal + '\n' +
                    tab + ' - codiCarrer: ' + adr.carrer.codi + '\n' +
                    tab + ' - codiIlla: ' + adr.illa.codi + '\n' +
                    tab + ' - codiDistricte: ' + adr.districte.codi + '\n' +
                    tab + ' - descripcioDistricte: ' + adr.districte.descripcio + '\n' +
                    tab + ' - codiBarri: ' + adr.barri.codi + '\n' +
                    tab + ' - nomBarri: ' + adr.barri.nom + '\n' +
                    tab + ' - codiPostal: 080' + adr.districtePostal + '\n' +
                    tab + ' - Solar: ' + adr.solar + '\n' +
                    tab + ' - Parcela: ' + adr.parcelaId + '\n' +
                    tab + ' - referenciaCadastral: ' + adr.referenciaCadastral + '\n' +
                    tab + ' - Secció censal: ' + adr.seccioCensal + '\n';

                text = text.replace(/\n/g, '<br/>');
                $('#out').html(text);
            }
        });
    });
}
//Cargamos ejercicios
window.onload = ejercicios();

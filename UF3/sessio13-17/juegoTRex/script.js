const dino = document.querySelector(".dino");
const cactus = document.querySelector(".cactus");

/**
 * Funcion para que tRex sale 
 * Se le añade una clase que previamente esta definida en el CSS
*/
function jump() {
    dino.classList.add("jump");
    //Quitar clase para volver a saltar, luego de que se cumpla la animacion
    setTimeout(() => {
        dino.classList.remove("jump")
    }, 500);
}

/**
 * Funcion para que tRex sale 
 * Se le añade una clase que previamente esta definida en el CSS
*/
function crouch() {
    dino.style.backgroundImage = "url(img/nube.png)"
}
document.addEventListener("keydown", function (e) {
    if (e.keyCode == 32) jump();
    if (e.keyCode == 40) crouch();
})



/**
 * Dectar colicion
 */
const juego = setInterval(() => {
    let dinoTop = parseInt(window.getComputedStyle(dino).getPropertyValue("top"));
    let captusLeft = parseInt(window.getComputedStyle(cactus).getPropertyValue("left"));
    if (captusLeft < 50 && captusLeft > 0 && dinoTop >= 140) {
        // cactus.classList.remove('cactus');
        // clearInterval(juego);
        // console.log("Fin juego");
    }
}, 10);
